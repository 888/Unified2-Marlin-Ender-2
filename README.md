# Unified2-Marlin-Ender-2

th3dstudio.com's version of Marlin firmware. Config for the Creality Ender 2 3D-printer. Using the Melzi Board (1284P, 16MHz, Sanguinololu V1.2 pin-out.) (This is NOT for the newer Ender 2 Pro)



[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)
